<?php
/**
 * @file
 *
 */

/**
 * Implements hook_views_data().
 */
function variables_everywhere_views_data() {
  $data = array();

  $data['variable_global']['table']['group'] = t('Variable');
  $data['variable_global']['table']['join'] = array(
    // #global let's it appear all the time.
    '#global' => array(),
  );

  $data['variable_global']['variable'] = array(
    'title' => t('Variable'),
    'help' => t('Displays the formatted value of a variable'),
    'area' => array(
      'handler' => 'variables_everywhere_handler_area',
    ),
  );

  $data['variable_global']['variable_field'] = array(
    'title' => t('Variable field'),
    'help' => t('Displays the formatted value of a variable'),
    'field' => array(
      'handler' => 'variables_everywhere_handler_field',
    ),
  );

  return $data;
}

/**
 * Implements hook_views_data_alter().
 */
function variables_everywhere_views_data_alter(&$data) {
  $handlers = variables_everywhere_get_views_handlers();
  $unset = array('field', 'sort', 'argument', 'relationship', 'area');
  foreach ($data as $table => $fields) {
    foreach ($fields as $field => $info) {
      if (is_array($info) && isset($info['filter']) && in_array($info['filter']['handler'], array_keys($handlers))) {
        // Create our variable based handler.
        $variables_everywhere_info = $info;
        foreach ($variables_everywhere_info as $key => $value) {
          if (in_array($key, $unset)) {
            unset($variables_everywhere_info[$key]);
          }
        }

        if (isset($info['title'])) {
          $variables_everywhere_info['title'] = $info['title'] . ' (config variable)';
        }
        if (isset($info['title short'])) {
          $variables_everywhere_info['title short'] = $info['title short'] . ' (config variable)';
        }
        $variables_everywhere_info['filter']['handler'] = $handlers[$info['filter']['handler']];
        // Keep or update our real field reference.
        if (isset($info['real field'])) {
          $variables_everywhere_info['real field'] = $info['real field'];
        }
        else {
          $variables_everywhere_info['real field'] = $field;
        }
        // Update filter title if provided.
        if (isset($info['filter']['title'])) {
          $variables_everywhere_info['filter']['title'] = $info['filter']['title'] . ' (config variable)';
        }
        $data[$table][$field . '_config_varibale'] = $variables_everywhere_info;
      }
    }
  }
}
