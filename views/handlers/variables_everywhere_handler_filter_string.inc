<?php
/**
 * @file
 *
 */

class variables_everywhere_handler_filter_string extends views_handler_filter_string {

  function value_form(&$form, &$form_state) {
    parent::value_form($form, $form_state);

    $form['value'] = array(
      '#title' => t('Variable'),
      '#type' => 'textfield',
      '#autocomplete_path' => 'variables_everywhere/autocomplete',
      '#default_value' => $this->value,
      '#required' => TRUE,
      '#description' => t('The variable containing the values to use.'),
    );
  }

  function query() {
    dpm(variable_get_value($this->value));
    $this->value = variable_get_value($this->value);
    parent::query();
  }

  function can_expose() {
    return FALSE;
  }

}
