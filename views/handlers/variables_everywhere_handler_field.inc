<?php
/*
 * @file
 */

class variables_everywhere_handler_field extends views_handler_field {
  function query() {
    // do nothing -- to override the parent query.
  }

  function admin_summary() {
    return check_plain($this->options['variable_name']);
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['variable_name'] = array('default' => '');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['variable_name'] = array(
      '#title' => t('Variable'),
      '#type' => 'textfield',
      '#autocomplete_path' => 'variables_everywhere/autocomplete',
      '#default_value' => isset($this->options['variable_name']) ? $this->options['variable_name'] : '',
      '#required' => TRUE,
      '#description' => t('The variable to show in this field.'),
    );
  }

  function render($empty = FALSE) {
    $variable_name = $this->options['variable_name'];

    return variable_format_value($variable_name);
  }
}
