<?php
/**
 * @file
 *
 */

class variables_everywhere_handler_filter_many_to_one extends views_handler_filter_many_to_one {

  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_options = variable_get_value($this->options['value']);
    }
    return $this->value_options;
  }

  function query() {
    $real_value = variable_get_value($this->value);

    if (is_array($real_value)) {
      $this->value = $real_value;
    }

    parent::query();
  }

  function can_expose() {
    return FALSE;
  }

  function value_form(&$form, &$form_state) {
    $form['value'] = array(
      '#title' => t('Variable'),
      '#type' => 'textfield',
      '#autocomplete_path' => 'variables_everywhere/autocomplete',
      '#default_value' => isset($this->options['value']) ? $this->options['value'] : '',
      '#required' => TRUE,
      '#description' => t('The variable containing the values to use.'),
    );

    if (empty($form_state['exposed'])) {
      $this->helper->options_form($form, $form_state);
    }
  }
}
