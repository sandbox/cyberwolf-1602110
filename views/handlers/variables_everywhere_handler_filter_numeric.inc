<?php
/**
 * @file
 *
 */

class variables_everywhere_handler_filter_numeric extends views_handler_filter_numeric {

  function value_form(&$form, &$form_state) {
    parent::value_form($form, $form_state);

    if (isset($form['value']['value'])) {
      $form['value']['value'] = array(
        '#title' => t('Variable'),
        '#type' => 'textfield',
        '#autocomplete_path' => 'variables_everywhere/autocomplete',
        '#default_value' => $this->value['value'],
        '#required' => TRUE,
        '#description' => t('The variable containing the values to use.'),
      );
    }
  }

  function query() {
    $this->value['value'] = variable_get_value($this->value['value']);
    parent::query();
  }

  function can_expose() {
    return FALSE;
  }

}
