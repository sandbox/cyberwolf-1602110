<?php
/**
 * @file
 * Defines a ctools content type in which you can include a variable.
 */

$plugin = array(
  'title' => t('Variable element'),
  'single' => TRUE,
  'category' => array(t('Variables')),
  'content type' => 'variable_element',
);

/**
 * Constructs the settings form.
 */
function variables_everywhere_variable_element_content_type_edit_form($form, &$form_state) {
  $form['variable'] = array(
    '#title' => t('Variable name'),
    '#type' => 'select',
    '#options' => variables_everywhere_handler_value_options(),
    '#default_value' => $form_state['conf']['variable'],
    '#required' => TRUE,
  );
  $form['variable_callback'] = array(
    '#title' => t('Variable formatting'),
    '#type' => 'select',
    '#options' => array(
      'variable_format_value' => t('Default'),
      'variable_get_value' => t('Plain'),
    ),
    '#default_value' => $form_state['conf']['variable_callback'],
    '#required' => TRUE,
  );

  return $form;
}

/**
 * Define a title for the panel content page.
 */
function variables_everywhere_variable_element_content_type_admin_title($subtype, $conf, $context) {
  return t('Variable element: "@s"', array('@s' => $conf['variable']));
}

/**
 * Submits the settings form.
 */
function variables_everywhere_variable_element_content_type_edit_form_submit($form, &$form_state) {
  $form_state['conf']['variable'] = $form_state['values']['variable'];
  $form_state['conf']['variable_callback'] = $form_state['values']['variable_callback'];
}

/**
 * Renders the variable's value.
 */
function variables_everywhere_variable_element_content_type_render($subtype, $conf, $args, $context, $incoming_content) {
  $block = new stdClass();

  // Fall back to the old behaviour if no variable callback is set.
  if (!isset($conf['variable_callback'])) {
    $conf['variable_callback'] = 'variable_format_value';
  }

  $block->content = $conf['variable_callback']($conf['variable']);
  $block->module = 'variables_everywhere';
  $block->delta  = 'variables-everywhere-' . $conf['variable'];
  $block->subtype  = 'variables-everywhere-' . $conf['variable'];

  return $block;
}
